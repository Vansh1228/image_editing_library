// import React, { useRef, useState, useEffect } from "react";
// import image from "../assets/Vansh Gupta PAN.jpg";
// import CommandManager from "../Commands/commandManager";
// import Toolbar from "./Toolbar";
// import Canvas from "./Canvas";
// import CropCommand from "../Commands/Concrete_Commands/crop";
// import RotateCommand from "../Commands/Concrete_Commands/rotate";
// import FlipCommand from "../Commands/Concrete_Commands/flip";
// import ZoomCommand from "../Commands/Concrete_Commands/zoom";

// import "react-toastify/dist/ReactToastify.css";
// function ImageEditor() {
//   const editorRef = useRef(null);
//   const commandManagerRef = useRef(new CommandManager(editorRef)).current;
//   const [quality, setQuality] = useState(0.8);
//   const [selectedFormat, setSelectedFormat] = useState("jpeg");
//   const [showDropdown, setShowDropdown] = useState(false);
//   const [rotateAngle, setRotateAngle] = useState(0);
//   const [isPanning, setIsPanning] = useState(false);
//   const [lastMousePosition, setLastMousePosition] = useState({ x: 0, y: 0 });
//   console.log(lastMousePosition);
//   useEffect(() => {
//     const canvas = editorRef.current.getCanvas();
//     const handleWheel = (event) => {
//       event.preventDefault();
//       const zoomScale = event.deltaY < 0 ? 1.1 : 0.9;
//       const zoomCommand = new ZoomCommand(
//         editorRef,
//         zoomScale,
//         event.offsetX,
//         event.offsetY
//       );
//       commandManagerRef.executeCommand(zoomCommand);
//     };
//     canvas.addEventListener("wheel", handleWheel);

//     const handleMouseDown = (event) => {
//       setIsPanning(true);
//       setLastMousePosition({ x: event.clientX, y: event.clientY });
//     };

//     const handleMouseMove = (event) => {
//       if (isPanning) {
//         const deltaX = event.clientX - lastMousePosition.x;
//         const deltaY = event.clientY - lastMousePosition.y;
//         const zoomCommand = new ZoomCommand(editorRef, 1, deltaX, deltaY, true);
//         commandManagerRef.executeCommand(zoomCommand);
//         setLastMousePosition({ x: event.clientX, y: event.clientY });
//       }
//     };

//     const handleMouseUp = () => {
//       setIsPanning(false);
//     };

//     canvas.addEventListener("mousedown", handleMouseDown);
//     window.addEventListener("mousemove", handleMouseMove);
//     window.addEventListener("mouseup", handleMouseUp);

//     return () => {
//       canvas.removeEventListener("wheel", handleWheel);
//       canvas.removeEventListener("mousedown", handleMouseDown);
//       window.removeEventListener("mousemove", handleMouseMove);
//       window.removeEventListener("mouseup", handleMouseUp);
//     };
//   }, [commandManagerRef, isPanning, lastMousePosition]);

//   const handleOperation = (tool) => {
//     console.log(tool);

//     if (tool === "crop") {
//       const cropCommand = new CropCommand(editorRef, {
//         x: 50,
//         y: 50,
//         width: 200,
//         height: 200,
//       });
//       commandManagerRef.executeCommand(cropCommand);
//     } else if (tool === "rotate") {
//       const rotateCommand = new RotateCommand(editorRef, 90);
//       commandManagerRef.executeCommand(rotateCommand);
//     } else if (tool === "flipX") {
//       const flipCommand = new FlipCommand(editorRef, "x");
//       commandManagerRef.executeCommand(flipCommand);
//     } else if (tool === "flipY") {
//       const flipCommand = new FlipCommand(editorRef, "y");
//       commandManagerRef.executeCommand(flipCommand);
//     }
//   };
//   const handleDownload = () => {
//     const canvas = editorRef.current.getCanvas();
//     const link = document.createElement("a");
//     link.download = `image.${selectedFormat}`;
//     const url = canvas.toDataURL(`image/${selectedFormat}`, quality);
//     link.href = canvas.toDataURL(`image/${selectedFormat}`, quality);
//     link.click();
//     console.log(url);
//   };
//   // useEffect(() => {
//   //   const rotateCommand = new RotateCommand(editorRef, rotateAngle);
//   //   commandManagerRef.executeCommand(rotateCommand);
//   //   console.log(rotateAngle);
//   // }, [rotateAngle]);
//   return (
//     <>
//       <div className="toolbar">
//         <Toolbar operation={handleOperation} />
//       </div>
//       <div className="image-editor">
//         <button onClick={() => commandManagerRef.undo()}>Undo</button>
//       </div>

//       <div className="dropdown">
//         <button onClick={() => setShowDropdown(!showDropdown)}>Submit</button>
//         {showDropdown && (
//           <div className="dropdown-content">
//             <div>
//               <input
//                 type="radio"
//                 id="jpeg"
//                 name="format"
//                 value="jpeg"
//                 checked={selectedFormat === "jpeg"}
//                 onChange={(e) => setSelectedFormat(e.target.value)}
//               />
//               <label htmlFor="jpeg">JPEG</label>
//             </div>
//             <div>
//               <input
//                 type="radio"
//                 id="png"
//                 name="format"
//                 value="png"
//                 checked={selectedFormat === "png"}
//                 onChange={(e) => setSelectedFormat(e.target.value)}
//               />
//               <label htmlFor="png">PNG</label>
//             </div>
//             <div>
//               <label htmlFor="quality">Quality:</label>
//               <input
//                 type="range"
//                 id="quality"
//                 name="quality"
//                 min="0.1"
//                 max="1"
//                 step="0.1"
//                 value={quality}
//                 onChange={(e) => setQuality(parseFloat(e.target.value))}
//               />
//             </div>
//             <button onClick={handleDownload}>Download</button>
//           </div>
//         )}
//       </div>
//       <div className="canvas-container">
//         <img id="source" src={image} alt="source" style={{ display: "none" }} />
//         <Canvas ref={editorRef} />
//         <div className="rotate-range">
//           <input
//             type="range"
//             min="0"
//             max="360"
//             value={rotateAngle}
//             onChange={(e) => setRotateAngle(parseInt(e.target.value))}
//           />
//         </div>
//       </div>
//     </>
//   );
// }

// export default ImageEditor;

// ImageEditor.tsx

import React, { useState, useEffect } from 'react';
import image from '../assets/Vansh Gupta PAN.jpg';
import Toolbar from './Toolbar';
import Canvas from './Canvas';
import CommandManager from '../Commands/commandManager';
import CropCommand from '../Commands/Concrete_Commands/crop';
import RotateCommand from '../Commands/Concrete_Commands/rotate';
import FlipCommand from '../Commands/Concrete_Commands/flip';
import ZoomCommand from '../Commands/Concrete_Commands/zoom';
import EditorManager from '../Commands/EditorManager';
import Command from '../Commands/command'; 

import 'react-toastify/dist/ReactToastify.css';

function ImageEditor() {
  const editorManager = EditorManager.getInstance();
  const editorRef = editorManager.getEditorRef();
  const commandManagerRef = editorManager.getCommandManagerRef();
  
  const [quality, setQuality] = useState(0.8);
  const [selectedFormat, setSelectedFormat] = useState('jpeg');
  const [showDropdown, setShowDropdown] = useState(false);
  const [rotateAngle, setRotateAngle] = useState(0);
  const [isPanning, setIsPanning] = useState(false);
  const [lastMousePosition, setLastMousePosition] = useState({ x: 0, y: 0 });

  useEffect(() => {
    const canvas = editorRef.current?.getCanvas();
    if (canvas) {
      const handleWheel = (event: WheelEvent) => {
        event.preventDefault();
        const zoomScale = event.deltaY < 0 ? 1.1 : 0.9;
        const zoomCommand = new ZoomCommand(
          editorRef,
          zoomScale,
          event.offsetX,
          event.offsetY
        );
        commandManagerRef.executeCommand(zoomCommand);
      };
      canvas.addEventListener('wheel', handleWheel);

      const handleMouseDown = (event: MouseEvent) => {
        setIsPanning(true);
        setLastMousePosition({ x: event.clientX, y: event.clientY });
      };

      const handleMouseMove = (event: MouseEvent) => {
        if (isPanning) {
          const deltaX = event.clientX - lastMousePosition.x;
          const deltaY = event.clientY - lastMousePosition.y;
          const zoomCommand = new ZoomCommand(editorRef, 1, deltaX, deltaY, true);
          commandManagerRef.executeCommand(zoomCommand);
          setLastMousePosition({ x: event.clientX, y: event.clientY });
        }
      };

      const handleMouseUp = () => {
        setIsPanning(false);
      };

      canvas.addEventListener('mousedown', handleMouseDown);
      window.addEventListener('mousemove', handleMouseMove);
      window.addEventListener('mouseup', handleMouseUp);

      return () => {
        canvas.removeEventListener('wheel', handleWheel);
        canvas.removeEventListener('mousedown', handleMouseDown);
        window.removeEventListener('mousemove', handleMouseMove);
        window.removeEventListener('mouseup', handleMouseUp);
      };
    }
  }, [commandManagerRef, isPanning, lastMousePosition]);

  const handleOperation = (tool: string, options: any) => {
    console.log(`Handling operation: ${tool} with options`, options);

    let command: Command | null = null;

    switch (tool) {
      case 'crop':
        cropOptions: {x:50; y:50; width:200; height: 200}
        command = new CropCommand(cropOptions);
        break;
      case 'rotate':
        command = new RotateCommand();
        break;
      case 'flipX':
      case 'flipY':
        command = new FlipCommand();
        break;
      default:
        break;
    }

    if (command) {
      commandManager.executeCommand(command, options);
    }
  };

  const handleDownload = () => {
    const canvas = editorRef.current?.getCanvas();
    if (canvas) {
      const link = document.createElement('a');
      link.download = `image.${selectedFormat}`;
      const url = canvas.toDataURL(`image/${selectedFormat}`, quality);
      link.href = url;
      link.click();
    }
  };

  return (
    <>
      <div className="toolbar">
        <Toolbar operation={handleOperation} />
      </div>
      <div className="image-editor">
        <button onClick={() => commandManagerRef.undo()}>Undo</button>
      </div>

      <div className="dropdown">
        <button onClick={() => setShowDropdown(!showDropdown)}>Submit</button>
        {showDropdown && (
          <div className="dropdown-content">
            <div>
              <input
                type="radio"
                id="jpeg"
                name="format"
                value="jpeg"
                checked={selectedFormat === 'jpeg'}
                onChange={(e) => setSelectedFormat(e.target.value)}
              />
              <label htmlFor="jpeg">JPEG</label>
            </div>
            <div>
              <input
                type="radio"
                id="png"
                name="format"
                value="png"
                checked={selectedFormat === 'png'}
                onChange={(e) => setSelectedFormat(e.target.value)}
              />
              <label htmlFor="png">PNG</label>
            </div>
            <div>
              <label htmlFor="quality">Quality:</label>
              <input
                type="range"
                id="quality"
                name="quality"
                min="0.1"
                max="1"
                step="0.1"
                value={quality}
                onChange={(e) => setQuality(parseFloat(e.target.value))}
              />
            </div>
            <button onClick={handleDownload}>Download</button>
          </div>
        )}
      </div>
      <div className="canvas-container">
        <img id="source" src={image} alt="source" style={{ display: 'none' }} />
        <Canvas />
        <div className="rotate-range">
          <input
            type="range"
            min="0"
            max="360"
            value={rotateAngle}
            onChange={(e) => setRotateAngle(parseInt(e.target.value))}
          />
        </div>
      </div>
    </>
  );
}

export default ImageEditor;

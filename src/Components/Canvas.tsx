import React, { forwardRef, useImperativeHandle, useRef, useEffect, useState, Ref } from "react";


type Transform = {
  scale: number;
  translateX: number;
  translateY: number;
};

interface CanvasProps extends React.CanvasHTMLAttributes<HTMLCanvasElement> {}

export interface CanvasHandle {
  getCanvas: () => HTMLCanvasElement | null;
  getContext: () => CanvasRenderingContext2D | null;
  getTransform: () => Transform;
  setTransform: (newTransform: Transform) => void;
}

const Canvas = forwardRef<CanvasHandle>((props, ref) => {
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const [scale, setScale] = useState(1);
  const [translateX, setTranslateX] = useState(0);
  const [translateY, setTranslateY] = useState(0);

  useImperativeHandle(ref, () => ({
    getCanvas: () => canvasRef.current,
    getContext: () => canvasRef.current?.getContext("2d") || null,
    getTransform: () => ({ scale, translateX, translateY }),
    setTransform: (newTransform: Transform) => {
      setScale(newTransform.scale);
      setTranslateX(newTransform.translateX);
      setTranslateY(newTransform.translateY);
    },
  }));

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas?.getContext("2d");
    const image = document.getElementById("source") as HTMLImageElement;

    if (image) {
      image.onload = () => {
        if (canvas && context) {
          canvas.width = image.naturalWidth;
          canvas.height = image.naturalHeight;
          context.drawImage(image, 0, 0);
        }
      };

      image.onerror = () => {
        console.error("Failed to load the image.");
      };
    }
  }, []);

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas?.getContext("2d");
    const image = document.getElementById("source") as HTMLImageElement;

    if (canvas && context && image) {
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.save();
      context.translate(translateX, translateY);
      context.scale(scale, scale);
      context.drawImage(image, 0, 0);
      context.restore();
    }
  }, [scale, translateX, translateY]);

  return <canvas ref={canvasRef} {...props} />;
});

export default Canvas;

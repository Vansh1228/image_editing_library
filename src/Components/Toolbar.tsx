import React from "react";

interface ToolbarProps {
  operation: (action: string) => void;
}

const Toolbar: React.FC<ToolbarProps> = ({ operation }) => {
  return (
    <div className="ToolBar">
      <button className="crop" onClick={() => operation("crop")}>Crop</button>
      <button onClick={() => operation("rotate")}>Rotate</button>
      <button onClick={() => operation("flipX")}>Flip Horizontal</button>
      <button onClick={() => operation("flipY")}>Flip Vertical</button>
      <button className="zoom" onClick={() => operation("zoom")}>Zoom</button>
    </div>
  );
}

export default Toolbar;

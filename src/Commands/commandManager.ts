import Command from "./command";

interface EditorRef {
  current: {
    getCanvas: () => HTMLCanvasElement;
    getContext: () => CanvasRenderingContext2D;
  };
}

interface HistoryItem {
  imageData: ImageData;
  width: number;
  height: number;
}

export default class CommandManager {
  private static instance: CommandManager;
  private history: { command: Command; options: any }[];
  private redoStack: { command: Command; options: any }[];

  private constructor() {
    this.history = [];
    this.redoStack = [];
  }

  static getInstance(): CommandManager {
    if (!CommandManager.instance) {
      CommandManager.instance = new CommandManager();
    }
    return CommandManager.instance;
  }
  executeCommand(command: Command): void {
    this._saveState();
    command.execute();
    this.redoStack = [];
    console.log(this.history);
  }

  undo(): void {
    console.log(this.history);
    if (this.history.length > 0) {

      const lastCommand = this.history.pop();
      if (lastCommand) {
        this.redoStack.push(lastCommand);
        this._restoreState();
      }
      console.log("undo history", this.history);
      console.log("redo stack", this.redoStack);
    }
  }

  redo(): void {
    const command = this.redoStack.pop();
    if (command) {
      command.execute();
      this.history.push(command);
    }
  }

  private _saveState(): void {
    const canvas = this.editorRef.current.getCanvas();
    const context = this.editorRef.current.getContext();
    if (canvas && context) {
      const imageData = context.getImageData(0, 0, canvas.width, canvas.height);
      this.history.push({
        imageData,
        width: canvas.width,
        height: canvas.height,
      });
    }
  }

  private _restoreState(): void {
    const lastHistoryItem = this.history[this.history.length - 1];
    if (lastHistoryItem) {
      const { imageData, width, height } = lastHistoryItem;
      const canvas = this.editorRef.current.getCanvas();
      const context = this.editorRef.current.getContext();
      if (canvas && context) {
        canvas.width = width;
        canvas.height = height;
        context.putImageData(imageData, 0, 0);
      }
    }
  }
}

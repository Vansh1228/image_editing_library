// EditorManager.ts
import React from 'react';
import CommandManager from '../Commands/commandManager';
import { CanvasHandle } from '../Components/Canvas'; // Ensure to import the correct type from your Canvas component

class EditorManager {
  private static instance: EditorManager;
  private editorRef: React.RefObject<CanvasHandle>;
  private commandManagerRef: CommandManager;

  private constructor() {
    this.editorRef = React.createRef<CanvasHandle>();
    this.commandManagerRef = new CommandManager(this.editorRef.current ?? undefined);
  }

  public static getInstance(): EditorManager {
    if (!EditorManager.instance) {
      EditorManager.instance = new EditorManager();
    }
    return EditorManager.instance;
  }

  public getEditorRef(): React.RefObject<CanvasHandle> {
    return this.editorRef;
  }

  public getCommandManagerRef(): CommandManager {
    return this.commandManagerRef;
  }
}

export default EditorManager;

import Command from "../command";

interface EditorRef {
  current: {
    getCanvas: () => HTMLCanvasElement | null;
    getContext: () => CanvasRenderingContext2D | null;
  };
}

export default class RotateCommand extends Command {
  private editorRef: EditorRef;
  private angle: number;
  private scale: number | null;

  constructor(editorRef: EditorRef, angle: number) {
    super();
    this.editorRef = editorRef;
    this.angle = angle;
    this.scale = null;
  }

  execute(): void {
    const canvas = this.editorRef.current.getCanvas();
    const context = this.editorRef.current.getContext();

    if (canvas && context) {
      const radians = this.angle * (Math.PI / 180);
      const { width, height } = canvas;

      const newWidth =
        Math.abs(Math.cos(radians) * width) +
        Math.abs(Math.sin(radians) * height);
      const newHeight =
        Math.abs(Math.sin(radians) * width) +
        Math.abs(Math.cos(radians) * height);

      if (!this.scale) {
        this.scale = Math.min(canvas.width / newWidth, canvas.height / newHeight);
      }

      const tempCanvas = document.createElement("canvas");
      const tempContext = tempCanvas.getContext("2d");
      if (tempContext) {
        tempCanvas.width = newWidth;
        tempCanvas.height = newHeight;

        tempContext.translate(newWidth / 2, newHeight / 2);
        tempContext.rotate(radians);
        tempContext.drawImage(canvas, -width / 2, -height / 2);

        context.clearRect(0, 0, canvas.width, canvas.height);
        context.save();   
        context.translate(width / 2, height / 2);
        context.scale(this.scale, this.scale);
        context.drawImage(tempCanvas, -newWidth / 2, -newHeight / 2);
        context.restore();
      }

      console.log("Rotate was executed");
    }
  }
}

import Command from "../command";

export default class FlipCommand extends Command {
  constructor(editorRef, direction) {
    super();
    this.editorRef = editorRef;
    this.direction = direction; // 'x' for horizontal flip, 'y' for vertical flip
  }

  execute() {
    const canvas = this.editorRef.current.getCanvas();
    const context = this.editorRef.current.getContext();

    // Save the current state
    this.prevState = context.getImageData(0, 0, canvas.width, canvas.height);

    // Perform the flip operation
    context.save();
    if (this.direction === 'x') {
      context.scale(-1, 1); // Flip horizontally
      context.drawImage(canvas, -canvas.width, 0);
    } else if (this.direction === 'y') {
      context.scale(1, -1); // Flip vertically
      context.drawImage(canvas, 0, -canvas.height);
    }
    context.restore();

    console.log(`Flip ${this.direction} was executed`);
  }

  undo() {
    if (this.prevState) {
      const canvas = this.editorRef.current.getCanvas();
      const context = this.editorRef.current.getContext();

      // Restore the previous state
      canvas.width = this.prevState.width;
      canvas.height = this.prevState.height;
      context.putImageData(this.prevState, 0, 0);

      console.log(`Flip ${this.direction} undo was executed`);
    }
  }
}

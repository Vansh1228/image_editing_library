import Command from "../command";
import CommandManager from "../commandManager";

export default class CropCommand implements Command {
  private cropOptions: { x: number; y: number; width: number; height: number };
  private prevState: ImageData | null;

  constructor(cropOptions: { x: number; y: number; width: number; height: number }) {
    this.cropOptions = cropOptions;
    this.prevState = null;
  }

  execute(commandManager: CommandManager): void {
    const canvas = commandManager.getCanvas();
    const context = commandManager.getContext();
    const { x, y, width, height } = this.cropOptions;

    // Save the current state
    this.prevState = context.getImageData(0, 0, canvas.width, canvas.height);

    // Perform the crop operation
    const imageData = context.getImageData(x, y, width, height);
    canvas.width = width;
    canvas.height = height;
    context.putImageData(imageData, 0, 0);
              
    console.log("Crop was executed");
  }


}

import Command from "../command";

interface EditorRef {
  current: {
    getCanvas: () => HTMLCanvasElement | null;
    getContext: () => CanvasRenderingContext2D | null;
    getTransform: () => { scale: number; translateX: number; translateY: number };
    setTransform: (transform: { scale: number; translateX: number; translateY: number }) => void;
  };
}

export default class ZoomCommand extends Command {
  private editor: EditorRef;
  private scale: number;
  private offsetX: number;
  private offsetY: number;
  private isPan: boolean;
  private prevState: { scale: number; translateX: number; translateY: number } | null;

  constructor(editor: EditorRef, scale: number, offsetX: number, offsetY: number, isPan: boolean = false) {
    super();
    this.editor = editor;
    this.scale = scale;
    this.offsetX = offsetX;
    this.offsetY = offsetY;
    this.isPan = isPan;
    this.prevState = null;
  }

  execute(): void {
    const canvas = this.editor.current.getCanvas();
    const context = this.editor.current.getContext();

    if (!canvas || !context) return;

    const { scale, translateX, translateY } = this.editor.current.getTransform();

    this.prevState = { scale, translateX, translateY };

    if (this.isPan) {
      this.editor.current.setTransform({
        scale,
        translateX: translateX + this.offsetX,
        translateY: translateY + this.offsetY,
      });
    } else {
      const newScale = scale * this.scale;
      const newTranslateX = translateX - (this.offsetX / scale) * (newScale - scale);
      const newTranslateY = translateY - (this.offsetY / scale) * (newScale - scale);

      this.editor.current.setTransform({
        scale: newScale,
        translateX: newTranslateX,
        translateY: newTranslateY,
      });
    }

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.save();
    context.translate(this.editor.current.getTransform().translateX, this.editor.current.getTransform().translateY);
    context.scale(this.editor.current.getTransform().scale, this.editor.current.getTransform().scale);
    const sourceImage = document.getElementById("source") as HTMLImageElement | null;
    if (sourceImage) {
      context.drawImage(sourceImage, 0, 0);
    }
    context.restore();
  }
}
